package com.multi.app.dto;

public class CreditRequest {
    private static long nextId = 1;

    private long id;
    private int age;
    private String name;
    private int sum;

    public void generateId() {
        this.id = nextId;
        nextId++;
    }

    public long getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
