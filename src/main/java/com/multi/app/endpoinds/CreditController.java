package com.multi.app.endpoinds;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.multi.app.dto.BooleanResponse;
import com.multi.app.dto.CreditRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
@Tag(name = "CREDIT", description = "API for credit")
@RestController
public class CreditController {
    private static final Map<Long, CreditRequest> db = new HashMap<>();

    @Operation(summary = "Create Credit by name and client")
    @PostMapping(path = "/credit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long addCredit(@RequestBody CreditRequest creditRequest) {
        if (!isValid(creditRequest)) {
            return 0L;
        }

        creditRequest.generateId();
        db.put(creditRequest.getId(), creditRequest);

        return creditRequest.getId();
    }

    @GetMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getCredit(@PathVariable("id") long id) {
        CreditRequest creditRequest = db.get(id);

        return creditRequest == null ? "\"Not found\"" : creditRequest;
    }

    @DeleteMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object deleteCredit(@PathVariable("id") long id) {
        CreditRequest creditRequest = db.remove(id);

        BooleanResponse br = new BooleanResponse(creditRequest != null);

        return br;
    }

    @GetMapping(path = "/credits", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getCredits() {
        return db.values();
    }

    private boolean isValid(CreditRequest creditRequest) {
        if (creditRequest == null) {
            return false;
        }

        if (creditRequest.getName() == null || creditRequest.getName().trim().length() == 0) {
            return false;
        }

        if (creditRequest.getAge() <= 0) {
            return false;
        }

        if (creditRequest.getSum() <= 0) {
            return false;
        }

        return true;
    }
}
