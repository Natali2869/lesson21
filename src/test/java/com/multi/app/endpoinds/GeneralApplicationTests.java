package com.multi.app.endpoinds;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GeneralApplicationTests {
    @LocalServerPort
    private Integer port;

    private final File swaggerSchemaJson = new File("src/test/resources/swagger-schema.json");

    @BeforeEach
    public void setup() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    public void swaggerJsonTesting() throws FileNotFoundException {
        Response response = RestAssured.given()
                .contentType("application/json")
                .get("/v2/api-docs");
        Assertions.assertEquals(200, response.statusCode());

        JSONObject swaggerJson = new JSONObject(response.getBody().print());
        JSONObject schemaJson = new JSONObject(new JSONTokener(new FileInputStream(swaggerSchemaJson)));

        Schema schema = SchemaLoader.load(schemaJson);
        schema.validate(swaggerJson);
    }
}
