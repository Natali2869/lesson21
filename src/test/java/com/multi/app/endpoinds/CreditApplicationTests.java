package com.multi.app.endpoinds;

import com.multi.app.dto.CreditRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CreditApplicationTests {
    @LocalServerPort
    private Integer port;

    @BeforeEach
    public void setup() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    @Order(1)
    public void createCreditRequestTesting() {
        // create json 1
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("age", 30);
        jsonObject1.put("name", "Anton");
        jsonObject1.put("sum", 1500);

        // check create 1
        Response response1 = RestAssured.given()
                .contentType("application/json")
                .body(jsonObject1.toString())
                .post("/credit");
        Assertions.assertEquals(200, response1.statusCode());
        Assertions.assertEquals(1L, response1.as(Long.class).longValue());

        // create json 2
        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("age", 20);
        jsonObject2.put("name", "Bob");
        jsonObject2.put("sum", 3000);

        // check create 2
        Response response2 = RestAssured.given()
                .contentType("application/json")
                .body(jsonObject2.toString())
                .post("/credit");
        Assertions.assertEquals(200, response2.statusCode());
        Assertions.assertEquals(2L, response2.as(Long.class).longValue());
    }

    @Test
    @Order(2)
    public void getCreditRequestTesting() {
        // check get existing record
        Response response1 = RestAssured.given().get("/credit/1");
        Assertions.assertEquals(200, response1.statusCode());
        Assertions.assertEquals(1L, response1.jsonPath().getLong("id"));

        // check get non-existing record
        Response response3 = RestAssured.given().get("/credit/3");
        Assertions.assertEquals(200, response3.statusCode());
        Assertions.assertEquals("Not found", response3.as(String.class));
    }

    @Test
    @Order(3)
    public void getCreditRequestsTesting() {
        // check get existing records
        Response response = RestAssured.given().get("/credits");
        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(2, response.jsonPath().getList("", CreditRequest.class).size());
    }

    @Test
    @Order(4)
    public void deleteCreditRequestTesting() {
        // check delete non-existing record
        Response response3 = RestAssured.given().delete("/credit/3");
        Assertions.assertEquals(200, response3.statusCode());
        Assertions.assertFalse(response3.jsonPath().getBoolean("success"));

        // check delete existing record
        Response response1 = RestAssured.given().delete("/credit/1");
        Assertions.assertEquals(200, response1.statusCode());
        Assertions.assertTrue(response1.jsonPath().getBoolean("success"));
    }
}
